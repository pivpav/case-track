# Case tracker

This is small application written on `Python` using `wxPython` which makes it work on `Linux`, `MacOS` and (potentially) `Windows`.

Application simply stays in pannel and shows you the cases left for your weekly goal.

Default goal is `9` (and it's maximum at the moment), but can be specified using `-c` or `--case-target` parameter.
